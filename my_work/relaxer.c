/* This module *is* SMP-safe */

/* Standard headers for LKMs */
#include <linux/kernel.h>
#include <linux/module.h>  

/* We also need the ability to put ourselves to sleep 
 * and wake up later. */
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/delay.h>
#include <linux/kernel_stat.h>
#include <linux/time.h>

#define DBG
#define KER_TIME 1
#define MAX_EXC 3 /* Assuming this maximum number of hogging processes */

/* The number of keyboard interrupt requests */
#define KB_IRQ kstat_irqs(1)

/* Returns true if a process has used more than KER_TIME seconds of CPU time */
#define XS_CPU(p) cputime_to_secs(p->stime) > KER_TIME

/* Returns true if a process has used more than 10% of the time since starting
 * in kernel routines */
#define XS_KER_TIME(p) cputime_to_secs(p->stime)*10 > \
(int)(ts.tv_sec - p->start_time.tv_sec)

/* Returns true if a process is not a kernel thread (i.e. p->mm != null) */
#define USR_PROC(p) p->mm != NULL

/* This is used by cleanup, to prevent the module from 
 * being unloaded while the kernel thread is still active */
static DECLARE_WAIT_QUEUE_HEAD(WaitQ);

/* function declarations */
static int worker_routine(void *);

static void check_stopped_procs(void);
static void prevent_busy(void);
static void stop_proc(struct task_struct* p);

/* variable declarations */
static int please_clock_off = 0; /* signal to thread */
static int worker_done = 0; /* flag for cleanup_process() */
static int kb_irq_prev = 0;
static int kb_irq_curr = 0; /* interrupt counts for keyboard */
static struct task_struct* p;
static struct timespec ts;
static struct stopped_task_struct
{
	struct task_struct *task;
	int cycles_to_resume;
} stopped_procs[MAX_EXC];

/* Iterates through the stopped processes and decrements their timers. If they 
 * have waited for a minute, resumes them. */
static void check_stopped_procs()
{
	int i;
	for(i = 0; i < MAX_EXC; i++)
	{
		/* Assuming task wasn't killed while it was stopped */
		if(stopped_procs[i].task != NULL)
		{
			if(stopped_procs[i].cycles_to_resume-- != 0)
			{
				/* still has to be stopped. */
				printk(
				"%s: Process %d stopped for %d more cycles\n",
					current->comm,
					stopped_procs[i].task->pid, 
					stopped_procs[i].cycles_to_resume+1);
				/* If the user has resumed the task, stop it 
				 * again. */
				send_sig(SIGSTOP, stopped_procs[i].task, 0);
			}
			else
			{
				/* time to resume the job */
				send_sig(SIGCONT, stopped_procs[i].task, 0);
				wake_up_process(stopped_procs[i].task);
				
				printk("%s: Process %d resumed\n",
						current->comm, 
						stopped_procs[i].task->pid);
				
				/* invalidate the entry in our array */
				stopped_procs[i].task = NULL;
				stopped_procs[i].cycles_to_resume = 0;
			}
		}
	}
	printk("%s: Exiting check.\n",
			current->comm);
}

static void stop_proc(struct task_struct* p)
{
	int i;
	for(i = 0; i < MAX_EXC; i++)
	{
		if(stopped_procs[i].task == p)
		{
			printk("%s: Process already stopped.\n",
					current->comm);
			goto end;
		}
	}
	for(i = 0; i < MAX_EXC; i++)
	{
		if(stopped_procs[i].task == NULL)
		{
			stopped_procs[i].task = p;
			stopped_procs[i].cycles_to_resume = 6;
			send_sig(SIGSTOP, stopped_procs[i].task, 0);
			goto end;
		}
	}
	printk("%s: Could not stop task: too many stopped already.\n",
			current->comm);
end:
	printk("%s: Exiting stop_proc().\n",
			current->comm);
}


/*  prevent_busy() will stop every process that uses excessive CPU resources */
static void prevent_busy()
{
	do_posix_clock_monotonic_gettime(&ts); /* get current time in seconds */
	printk("%s: Current time in seconds: %d.\n", 
			current->comm,
			(int)ts.tv_sec);
 	for_each_process(p) /* iterates through the task list */
 	{
 		if(XS_CPU(p) && XS_KER_TIME(p) && USR_PROC(p))
 		{
 			printk("%s: Process %d has exceeded normal CPU\
 usage. Suspending for 1 minute.\n", 
 					current->comm,
 					(int)p->pid);
 			stop_proc(p);
 		}
 	}
}

/* This is the routine that is run by the kernel thread we
   start when the modulue is loaded */
static int worker_routine(void *irrelevant)
{
	/* drop userspace stuff */
	daemonize("relaxer");
	printk("%s: Relaxer initialised.\n",
			current->comm);	
	/* initialise kb_irq_prev */
	kb_irq_prev = KB_IRQ;
	/* now do the work */
	while (1) {
		//    mdelay(1000);
		/* If cleanup wants us to die */
		if (please_clock_off) 
		{
			printk("%s: Relaxer shutting down.\n",
					current->comm);
			worker_done = 1;   /* Set flag to wake cleanup_module */
			wake_up(&WaitQ);   /* Now cleanup_module can return */
			complete_and_exit(NULL,0);  /* terminate the k thread */
		}
		else
		{
			/* do some work */
			kb_irq_curr = KB_IRQ;
			if(kb_irq_curr - kb_irq_prev > 0)
			{
				prevent_busy();
			}
			kb_irq_prev = kb_irq_curr;
			
			/* If there are stopped processes, remove 10 seconds
			   from their clock and resume accordingly */
			check_stopped_procs();
			
			printk("%s: Pass complete.\n",
					current->comm);
			/* schedule timeout will busy wait unless 
			 * we say otherwise */
			/* sleep for 10 seconds */
			set_current_state(TASK_INTERRUPTIBLE);
			schedule_timeout(10*HZ);
		}
	}
}

/* Initialize the module - start kernel thread */
int init_module()
{
	kernel_thread(worker_routine,NULL,0);
	return 0;
}


/* Cleanup */
void cleanup_module()
{
	please_clock_off = 1;
	/* this 12 second sleep simulates the effect of having an SMP
	   system, where the worker thread might be executing at the same
	   time as rmmod */
#ifdef DBG
	set_current_state(TASK_UNINTERRUPTIBLE);
	schedule_timeout(12*HZ);
#endif
	/* Wait for the worker to notice that we're waiting, and exit */
	wait_event(WaitQ, worker_done);
}

MODULE_LICENSE("GPL");
